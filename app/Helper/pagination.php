<?php


namespace App\Helper;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class pagination
{
    public static function paginate($items , $count ,$perPage)
    {

        return new LengthAwarePaginator($items, $count, $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }
}
