<?php


namespace App\Http\Controllers;


use App\Helper\pagination;
use Illuminate\Support\Facades\DB;

trait getResponseDynamic
{

    /**
     * inputs information
     *
     *
     * @param $inputs
     */
    public function getResponse($inputs)
    {
        $query = DB::table($inputs['tableName']);

        $columns = [];
        $types = [];
        foreach ($inputs['columns'] as $item) {
            $explode = explode(':', $item);
            $columns[] = $explode[0];
            $types[] = $explode[1];
        }

        $query->select($columns, $types);
        if ($inputs['search'] != null) {
            $search = $inputs['search'];
            foreach ($inputs['filedSearch'] as $item) {
                $query->orWhere($item, 'LIKE', '%' . $search . '%');
            }
        }
        $query = $query->get();

        $response = [];
        foreach ($query as $item) {
            $response['columns'] = $columns;
            $response['data'][] = $this->object($item, $types);
            $response['filedSearch'] = $inputs['filedSearch'];
        }

        $response = pagination::paginate($response, \count($query), $inputs['paginate'])->appends(request()->query());
        return $response;
    }

    /**
     * append type in column
     *
     *
     * @param $data
     * @return \stdClass
     */
    private function object($data, $types)
    {
        $column = [];
        $index = 0;
        foreach ($data as $key => $item) {
            $column[] =  [
                    $item,
                    $types[$index]
            ];
            $index++;
        }
        return $column;
    }
}
