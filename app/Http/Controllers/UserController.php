<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Utils;

class UserController extends Controller
{
    use getResponseDynamic;
    /**
     * get list users
     *
     *
     */
    public function UserList(Request $request ,$search = null)
    {
        $inputs = [
            'tableName' => 'users',
            'columns' => [
                'id:integer',
                'name:string',
                'family:string',
                'email:string',
                'score:float',
                'status:boolean',
                'phone:integer',
                'created_at:timestamp'
            ],
            'paginate' => 5,
            'search' => $request->search != null ? $request->search : '',
            'filedSearch' => [
                'name',
                'family',
                'email',
            ]
        ];

        $response = $this->getResponse($inputs);
        return view('index' , compact('response'));
    }
}
