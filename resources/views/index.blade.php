<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        a {
            float: right;
            margin-right: 20px;
            margin-top: 20px;
            margin-bottom: 10px;
        }
        .alert{
            position: relative;
            top: 73px;
        }
    </style>
</head>
<body>

<a href="{{route('user.list')}}" class="btn btn-primary">user list</a>
@if(isset($response))
    @if($response->toArray()['data'] == [])
        <div class="alert alert-danger">not found</div>
    @else
        <form method="get" action="{{route('user.list')}}">
            <div class="input-group">
                <input type="search" class="form-control rounded" name="search" placeholder="@foreach($response['filedSearch'] as $item) {{$item }} @endforeach" aria-label="Search"
                       aria-describedby="search-addon" />
                <button type="submit" class="btn btn-outline-primary">search</button>

            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                @foreach($response['columns'] as $columnName)
                    <th scope="col">{{$columnName}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($response['data'] as $item)
                <tr>
                    @foreach($item as $column)
                        <th scope="row">{{$column[0] }}</th>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@endisset
</body>
</html>
